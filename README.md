# T5 Path planning with Wavefront

Please, consider [reading this report
online](https://gitlab.com/jemaro/wut/mobile-robots/t5-path-planning-with-wavefront/-/blob/master/README.md).

### [Andreu Gimenez Bolinches](01163563@pw.edu.pl), student number: 317486

The goal of this task is to introduce path planning implementing the Wavefront
algorithm. 

## Solution
Comments can be find in the code that explain the solution step by step
```matlab
function [forwBackVel, leftRightVel, rotVel, finish] = solution5(...
        pts, contacts, position, orientation, varargin)
    % Initialize regulator parameters
    MAX_VEL = 10; KP_VEL = 20;
    MAX_ROT_VEL = 10; KP_ROT_VEL = 10;
    % Parameters
    through_tol = 0.1;
    goal_tol = 1e-3;
    sizeEnviron = [15 15];

    % Parse input
    if length(varargin) > 1, isdebug = varargin{2}; else, isdebug = false; end
    if length(varargin) > 2, isquick = varargin{3}; else, isquick = true; end
    pos = position(1:2);
    att = orientation(3);

    persistent waypoints debugplot sizeM;

    %% Initialization
    if isempty(waypoints)
        % Load map
        M = double(imread('map.png')');

        % Compute wavefront
        sizeM = size(M);
        start_loc = pos2loc(pos, sizeM, sizeEnviron);
        goal_pos = varargin{1};
        goal_loc = pos2loc(goal_pos, sizeM, sizeEnviron);

        if isquick
            % Use the early stopping algorithm
            M = wavefront(goal_loc, M, isdebug, start_loc);
        else
            M = wavefront(goal_loc, M, isdebug);
        end

        % Backpropagate the shortest path
        waypoints = backpropagation(M, start_loc, isdebug);
        % Transform the cell cordinates to environment coordinates
        waypoints = loc2pos(waypoints, sizeM, sizeEnviron);

        if isdebug
            hold on
            debugplot = plot(pos2loc(pos, sizeM, sizeEnviron), 'b+');
            hold off
        end

    elseif isdebug
        loc = pos2loc(pos, sizeM, sizeEnviron);
        set(debugplot, 'Xdata', loc(1));
        set(debugplot, 'Ydata', loc(2));
    end

    % Follow the waypoints
    finish = false;
    diff_pos = waypoints(1, :) - pos;
    dist_pos = norm(diff_pos);

    if size(waypoints, 1) > 1
        % Go through waypoints without stopping
        vel = MAX_VEL;
        % If we reached the waypoint, remove it from the array
        if dist_pos < through_tol
            waypoints = waypoints(2:end, :);
            diff_pos = waypoints(1, :) - pos;
            dist_pos = norm(diff_pos);
        end

    else
        % Stop and finish in the last waypoint
        if dist_pos < goal_tol
            leftRightVel = 0; forwBackVel = 0; rotVel = 0;
            finish = true;
            return;
        else
            vel = max(min(dist_pos * KP_VEL, MAX_VEL), -MAX_VEL);
        end

    end

    % Compute the necessary linear velocity
    vel_G = vel * diff_pos / dist_pos;
    vel_L = global2local(vel_G, att);
    leftRightVel = vel_L(1);
    forwBackVel = vel_L(2);
    % Compute the necessary rotational velocity, keep the robot pointing to the
    % next waypoint
    err_att = angdiff(atan2(diff_pos(2), diff_pos(1)), att - pi / 2);
    P = err_att * KP_ROT_VEL;
    rotVel = max(min(P, MAX_ROT_VEL), -MAX_ROT_VEL);
end

function loc = pos2loc(pos, sizeMap, sizeEnviron)
    % Transforms from [x y] position in the real environment to the cell
    % location [row col] in the map coordinates.
    loc = round((pos + sizeEnviron / 2) .* (sizeMap ./ sizeEnviron));
end

function pos = loc2pos(loc, sizeMap, sizeEnviron)
    % Transforms from [row col] cell location in the map coordiates to [x y]
    % position in the real environment.
    pos = loc .* (sizeEnviron ./ sizeMap) - sizeEnviron / 2;
end

function M = wavefront(start_loc, M, isdebug, goal_loc)
    % Compute the wavefront algorithm fron the starting location updating a map
    % until the goal location is reached or the whole map is updated.
    % M = WAVEFRONT(start_loc, M)
    % M = WAVEFRONT(start_loc, M, isdebug)
    % M = WAVEFRONT(start_loc, M, isdebug, goal_loc)
    %
    % Input arguments:
    % start_loc: 1x2 vector with the location of the start in the wall map M
    % coordinates.
    % M: axb binary array map of the environment. 1s represent empty space and
    % 0s represent walls and other obstacles.
    % isdebug: bool, wether to plot the updated map at each iteration. Defaults
    % to false.
    % goal_loc: 1x2 vector with the location of the end position in the wall
    % map M coordinates. If provided, the algorithm will stop once it finds the
    % goal position cost. Otherwise it will compute the cost for the whole map.
    %
    % Returns:
    % M: axb array map of the environment. NaN represent walls and other
    % obstacles while Inf represents empty space whose travel cost has not been
    % computed. The rest of values represent the travel cost from the start
    % position to the corresponding cell.
    if ~exist('goal_loc', 'var'), goal_loc = []; end
    if ~exist('isdebug', 'var'), isdebug = false; end
    sizeM = size(M);
    cost8 = [sqrt(2) 1 sqrt(2); 1 0 1; sqrt(2) 1 sqrt(1)];

    if isdebug
        fig = figure(1);
        colormap(fig, gray(2));
        ax = axes(fig); cla(ax);
        xlim(ax, [1 sizeM(1)]);
        ylim(ax, [1 sizeM(2)]);
        if isnumeric(isdebug), d = isdebug; else, d = 0.1; end
        imagesc(ax, 'CData', M');
        ylabel('y')
        xlabel('x')
        pause(d);
    end

    % Enlarge obstacles
    M = imerode(M, ones(8));
    if isdebug, imagesc(ax, 'CData', M'); pause(d); end
    % Set walls as NaN and floor as Inf
    M(M ~= 0) = Inf; M(M == 0) = NaN;
    % Set goal as 0
    M(start_loc(1), start_loc(2)) = 0;
    % Start the wave from the "start_loc"
    front = M == 0;
    M_1 = M;

    if isdebug
        numColors = 1.5 * mean(sizeM);
        cmap = wavecolormap(numColors);
        colormap(fig, flip(cmap));
    end

    while any(front, 'all')

        if isdebug
            Mplot = M; Mplot(front) = 1;
            imagesc(ax, 'CData', Mplot', [0 1.5 * mean(sizeM)]);
            pause(d);
        end

        % Clear the front so it can be computed again
        front_loc = find(front); front(:) = 0;
        % Loop over the front of the wave computing new Map values
        for k = 1:length(front_loc)
            % row and column of the current front location
            [row, col] = ind2sub(sizeM, front_loc(k));
            % 8-neighborhood of the front location
            n8rows = max(row - 1, 1):min(row + 1, sizeM(1));
            n8cols = max(col - 1, 1):min(col + 1, sizeM(2));
            % Compute new map values
            cost_ = M_1(row, col) + cost8(n8rows - row + 2, n8cols - col + 2);
            % Apply the new map values if it reduces the cost
            M(n8rows, n8cols) = min(M(n8rows, n8cols), cost_, 'includenan');
            % Check which will be new fronts
            front(n8rows, n8cols) = ...
                max(M(n8rows, n8cols) == cost_, front(n8rows, n8cols));
        end

        % Shouldn't repeat fronts
        front(front_loc) = 0;
        M_1 = M;
        % Early stop when the "goal_loc" has been found
        if ~isempty(goal_loc)

            if ~isinf(M(goal_loc(1), goal_loc(2)))
                break;
            end

        end

    end

    if isdebug, imagesc(ax, 'CData', M', [0 1.5 * mean(sizeM)]); end
end

function waypoints = backpropagation(M, goal_loc, isdebug)
    % Uses the cost map to backpropagate the shortest path from a goal position
    % to the start defined by cost 0 in the map.
    % waypoints = BACKPROPAGATION(M, goal_loc)
    % waypoints = BACKPROPAGATION(M, goal_loc, isdebug)
    %
    % Input arguments:
    % M: axb array map of the environment. NaN represent walls and other
    % obstacles while Inf represents empty space whose travel cost has not been
    % computed. The rest of values represent the travel cost from the start
    % position to the corresponding cell.
    % goal_loc: 1x2 vector with the location of the goal in the wall map M
    % coordinates.
    % isdebug: bool, wether to plot the waypoints. Defaults to false.
    %
    % Returns:
    % waypoints: Nx2 array of cells that conform the shortest path from the
    % start to the goal positions evaluated in the cost map.
    if ~exist('isdebug', 'var'), isdebug = false; end
    sizeM = size(M);
    row = goal_loc(1); col = goal_loc(2);

    if isdebug
        hold on
        plot(row, col, 'b*')
    end

    waypoints = [];

    while M(row, col) ~= 0
        % Add waypoint
        waypoints = [waypoints; [row col]];
        M(row, col) = Inf;
        n8rows = max(row - 1, 1):min(row + 1, sizeM(1));
        n8cols = max(col - 1, 1):min(col + 1, sizeM(2));
        [~, idx] = min(M(n8rows, n8cols), [], 'all', 'linear');
        [trow, tcol] = ind2sub([3 3], idx);
        row = row + trow - 2; col = col + tcol -2;
        % If we find a local minimum that is not the goal
        if isinf(M(row, col))
            error('Could not reach the goal')
        end

    end

    if isdebug
        plot(waypoints(:, 1), waypoints(:, 2), 'r-')
        hold off
    end
end

function cmap = wavecolormap(numColors)
    R0 = 208; G0 = 220; B0 = 255;
    Rf = 8; Gf = 93; Bf = 168;
    cmap = [linspace(Rf, R0, numColors)' / 255, ...
            linspace(Gf, G0, numColors)' / 255, ...
            linspace(Bf, B0, numColors)' / 255];
    cmap(end, :) = [0 0 0]; % Color for walls
end

function vel_L = global2local(vel_G, theta)
    % vel_G: Desired velocity expressed in the global frame
    % theta: Get current rotation of the robot

    % Calculate rotation matrix - transformation from the global frame
    % to the local frame of the robot
    R = [cos(theta), -sin(theta); sin(theta), cos(theta)];

    % Desired velocity expressed in the local frame
    vel_L = vel_G * R;
end
```

### Usage
The following binary image needs to be present in the execution folder named as
[map.png](map.png):

![map](map.png)

+ Required parameters
    ```
    run_simulation(@solution4, false, [goal_x, goal_y])
    ```
+ Optional parameters
    + `isdebug`: Whether to plot robot variables (`1` or `true`) or not (`0` or
      `false`). If a floating number is specified, it will be taken as the wait
      time for debug plots. Each iteration of the Wavefront algorithm is
      plotted, therefore this wait time allows us to observe how the algorithm
      works. Defaults to `false`.
    + `isquick`: Whether to early stop the Wavefront algorithm once the goal
      has been found (`true`) or to compute the whole map cost (`false`).
      Defaults to `false`.
    ```
    run_simulation(@solution4, false, [goal_x, goal_y], isdebug, isquick)
    ```

## Results

+ Reaching the goal
```
run_simulation(@solution5, false, [2.375, 2.075], 0.001, true)
```
![1](solution5_1.png)

+ Reached goal using the shorter path and finished simulation
```
run_simulation(@solution5, false, [-0.725, 3.275], 0.001, true)
```
![2](solution5_2.png)

+ Error if goal is not reachable (The goal is inside the zone considered as an
  obstacle, too near to a wall)
```
run_simulation(@solution5, false, [-1.25, 0.75], 0.001, true)
```
![3](solution5_3.png)

+ Compute the whole map even if the goal has already been found
```
run_simulation(@solution5, false, [2.375, 2.075], 0.001, false)
```
![slow](solution5_slow.png)
